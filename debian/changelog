libmspub (0.1.4-4) UNRELEASED; urgency=medium

  * add one more #include <cstdint> to fix build with g++ 15
    (closes: #1097233) 

 -- Rene Engelhard <rene@debian.org>  Mon, 17 Feb 2025 23:03:20 +0100

libmspub (0.1.4-3) unstable; urgency=medium

  * oops, reupload without debugging CXX=g++-10 setting 

 -- Rene Engelhard <rene@debian.org>  Sat, 18 Apr 2020 07:07:32 +0200

libmspub (0.1.4-2) unstable; urgency=medium

  * #include <cstdint> in src/lib/MSPUBMetaData.h to fix build with g++ 10
    (closes: #957448)

 -- Rene Engelhard <rene@debian.org>  Fri, 17 Apr 2020 20:57:30 +0000

libmspub (0.1.4-1) unstable; urgency=medium

  * New upstream version 0.1.4

 -- Rene Engelhard <rene@debian.org>  Sat, 03 Mar 2018 19:23:32 +0000

libmspub (0.1.3-1) unstable; urgency=medium

  * New upstream version 0.1.3

 -- Rene Engelhard <rene@debian.org>  Sun, 31 Dec 2017 18:34:58 +0100

libmspub (0.1.2-4) unstable; urgency=medium

  * [511ffdd] add missing libicu-dev and zlib1g-dev to libmspub-devs Depends:

 -- Rene Engelhard <rene@debian.org>  Tue, 27 Sep 2016 01:19:59 +0200

libmspub (0.1.2-3) unstable; urgency=medium

  * [27793ad] move Maintainer: to Debian LibreOffice Maintainers
  * [2fb63bf] multiarchify
  * [a328219] fix cut-and-paste error in package description (closes: #774909)

 -- Rene Engelhard <rene@debian.org>  Mon, 12 Sep 2016 22:05:57 +0200

libmspub (0.1.2-2) unstable; urgency=medium

  * upload to unstable

 -- Rene Engelhard <rene@debian.org>  Sun, 05 Apr 2015 19:13:48 +0200

libmspub (0.1.2-1) experimental; urgency=medium

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Tue, 30 Dec 2014 19:49:12 +0100

libmspub (0.1.1-2) unstable; urgency=low

  * upload to unstable 

 -- Rene Engelhard <rene@debian.org>  Thu, 07 Aug 2014 23:45:53 +0200

libmspub (0.1.1-1) experimental; urgency=low

  * New upstream release

  * fix description (closes: #712168) 
  * fix copyright after relicensing (closes: #750802)

 -- Rene Engelhard <rene@debian.org>  Tue, 01 Jul 2014 19:27:01 +0200

libmspub (0.1.0-1) experimental; urgency=low

  * New upstream release
  
  * use dh-autoreconf (closes: #748280)

 -- Rene Engelhard <rene@debian.org>  Fri, 23 May 2014 20:51:06 +0200

libmspub (0.0.6-1) unstable; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Mon, 13 May 2013 18:39:21 +0200

libmspub (0.0.5-3) unstable; urgency=low

  * upload to unstable 

 -- Rene Engelhard <rene@debian.org>  Thu, 18 Apr 2013 22:52:14 +0200

libmspub (0.0.5-2) experimental; urgency=low

  * apply patche from Benjamin Drung:
    - actually set --docdir=/usr/share/doc/libmspub-doc 

 -- Rene Engelhard <rene@debian.org>  Mon, 04 Mar 2013 20:17:37 +0100

libmspub (0.0.5-1) experimental; urgency=low

  * New upstream release

  * apply patches from Benjamin Drung:
    - enable hardening
    - set docdir instead of fixing the installation directory afterwards
    - lintian fixes, Standards-Version: 3.9.4 (no changes needed)
    - add homepage field.
    - Add watch file.
    - Fix source URL.
  * --disable-silent-rules

 -- Rene Engelhard <rene@debian.org>  Wed, 27 Feb 2013 21:55:12 +0100

libmspub (0.0.4-1) experimental; urgency=low

  * New upstream release

 -- Rene Engelhard <rene@debian.org>  Tue, 29 Jan 2013 20:49:51 +0100

libmspub (0.0.3-1) experimental; urgency=low

  * Initial release

 -- Rene Engelhard <rene@debian.org>  Fri, 23 Nov 2012 20:26:56 +0100
